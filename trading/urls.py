from django.urls import path

from .views import *


app_name = 'trading'

urlpatterns = [
    path('investments/<int:pk>/receipt/', receipt_view, name='receipt'),
    path('investments/', investment_view, name='investments'),
    path('withdraw/', withdrawals_view, name='withdraw'),
    path('sell/', sell_view, name='sell'),
    path('sell/submit/', sell_shares, name='sell_submit'),
    path('buy/', buy_shares, name='buy_shares'),
]
