from django.contrib import admin

from .models import Investment, Withdrawal


class InvestmentAdmin(admin.ModelAdmin):

    list_display = ('user', 'amount', 'investment_date')

    search_fields = ('user__username',)


class WithdrawalRequestAdmin(admin.ModelAdmin):

    list_display = ('user', 'amount', 'date_of_approval', 'status')
    list_filter = ('status',)
    search_fields = ('user__username',)


admin.site.register(Investment, InvestmentAdmin)
admin.site.register(Withdrawal, WithdrawalRequestAdmin)
