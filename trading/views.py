from core.utils import Account
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.db.models import Sum
from django.http import JsonResponse, HttpResponse, HttpResponseBadRequest
from django.shortcuts import render, redirect
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt

from core.models import Share
from trading.models import Investment, Withdrawal, SoldShares


@csrf_exempt
def buy_shares(request):
    if request.is_ajax():
        print(f'Incoming Request: {request.POST}')
        quantity = int(request.POST['quantity'])
        fees = int(request.POST['fees'])
        symbol = request.POST['symbol']

        share = Share.objects.get(symbol=symbol)
        user = request.user

        if quantity < 1:
            return HttpResponse('Shares bought cannot be 0',
                                status=HttpResponseBadRequest.status_code)

        unit_price = share.current_price
        amount = unit_price * quantity

        if amount + fees > user.wallet.amount:
            return HttpResponse('Not enough balance to invest this amount. Try a lower amount or deposit first',
                                status=HttpResponseBadRequest.status_code)
        investment = Investment.objects.create(
            user=user,
            amount=amount,
            share=share,
            quantity=quantity,
            fees=fees,
            current_price=share.current_price
        )
        return JsonResponse(data=serializers.serialize('json', [investment, ]), safe=False)


@login_required
def investment_view(request):
    invs = Investment.objects.filter(user=request.user)
    context = {
        'investments': invs
    }
    return render(request, 'trading/investments.html', context)


@login_required
def receipt_view(request, pk):
    try:
        inv = Investment.objects.get(pk=pk)
    except Investment.DoesNotExist:
        return redirect('trading:investments')
    return render(request, 'trading/receipt.html', {'investment': inv})


@login_required
def withdrawals_view(request):
    if request.method == 'POST':
        amount = int(request.POST['amount'])
        if 0 < amount <= request.user.wallet.amount:
            Withdrawal.objects.create(
                user=request.user,
                amount=amount,
            )
            messages.success(request, f'Successfully Withdrawn Ksh. {amount}')
            return redirect('trading:withdraw')
        else:
            messages.warning(request, "Invalid amount or not enough balance. Please try again later.")
    w = Withdrawal.objects.filter(user=request.user)
    context = {
        'withdrawals': w
    }
    return render(request, 'trading/withdrawals.html', context=context)


@csrf_exempt
def sell_shares(request):
    if request.is_ajax():
        asset = request.POST['asset']
        quantity = int(request.POST['quantity'])
        try:
            share = Share.objects.get(symbol=asset)
        except Share.DoesNotExist:
            return HttpResponse('Invalid Asset Selected.', HttpResponseBadRequest.status_code)
        # asst_qty = request.user.investment_set.filter(share=share).aggregate(Sum('quantity'))['quantity__sum'] or 0
        # print(asst_qty)
        acc = Account(request.user)
        asst_qty = 0
        for ast in acc.balance():
            if ast['share__symbol'] == share.symbol:
                asst_qty = ast['total_items']
                # print(f"asset {ast['share_symbol']} -> {ast['total_items']}")
                
        if 0 < quantity <= asst_qty:
            SoldShares.objects.create(
                user=request.user,
                share=share,
                quantity=quantity,
                amount_received=share.current_price * quantity
            )
            return HttpResponse(
                f"Successfully sold {quantity} {share.name} shares Worth Ksh. {share.current_price * quantity}",
                200
            )
        return HttpResponse(
            f"Invalid quantity. Either you dont have enough {share.name} shares or you entered invalid amount. Try again",
            HttpResponseBadRequest.status_code
        )


@login_required
def sell_view(request):
    acc = Account(request.user)
    items = [i for i in acc.balance() if i['total_items'] > 0]
    names = [i['share__symbol'] for i in items]

    context = {
        'shares': items,
        'names': names,
        'value': acc.value()
    }
    return render(request, 'trading/sell.html', context)
