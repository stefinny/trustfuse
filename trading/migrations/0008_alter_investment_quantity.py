# Generated by Django 3.2.7 on 2021-09-05 10:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('trading', '0007_soldshares_amount_received'),
    ]

    operations = [
        migrations.AlterField(
            model_name='investment',
            name='quantity',
            field=models.IntegerField(default=1),
        ),
    ]
