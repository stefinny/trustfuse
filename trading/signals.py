from django.db.models.signals import post_save
from django.dispatch import receiver

from trading.models import Investment, Withdrawal, SoldShares
from wallet.models import WalletTransaction, WalletTransactionType


@receiver(post_save, sender=Investment)
def investment_transaction(sender, instance, created, **kwargs):
    if created:
        WalletTransaction.objects.create(
            user=instance.user,
            amount=instance.amount + instance.fees,
            transaction_type=WalletTransactionType.INVESTMENT
        )


@receiver(post_save, sender=Withdrawal)
def withdrawal_transaction(sender, instance, created, **kwargs):
    if created:
        WalletTransaction.objects.create(
            user=instance.user,
            amount=instance.amount,
            transaction_type=WalletTransactionType.WITHDRAWAL
        )


@receiver(post_save, sender=SoldShares)
def sell_transaction(sender, instance, created, **kwargs):
    if created:
        WalletTransaction.objects.create(
            user=instance.user,
            amount=instance.amount_received,
            transaction_type=WalletTransactionType.SOLD
        )
