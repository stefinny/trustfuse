from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from core.models import Share


class WithdrawStatus(models.IntegerChoices):

    PENDING = 0, _('Pending')
    APPROVED = 1, _('Approved')
    COMPLETED = 2, _('Completed')
    REJECTED = 3, _('Rejected')


class Base(models.Model):
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Investment(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    share = models.ForeignKey(Share, on_delete=models.CASCADE, default=1)
    quantity = models.IntegerField(default=1)
    amount = models.FloatField()
    fees = models.FloatField(default=0)
    current_price = models.FloatField(default=0)
    investment_date = models.DateTimeField(default=timezone.now)

    def cost(self):
        return self.current_price * self.quantity

    def total_paid(self):
        return self.cost() + self.fees

    def net_earning(self):
        share_value = self.share.current_price
        return (share_value * self.quantity + self.fees) - (self.total_paid())


class Withdrawal(Base):
    user = models.ForeignKey(to=User, on_delete=models.CASCADE)
    amount = models.FloatField()
    status = models.IntegerField(choices=WithdrawStatus.choices, default=WithdrawStatus.PENDING)
    status_by = models.ForeignKey(to=User, on_delete=models.CASCADE, related_name='approver', null=True, blank=True)
    date_of_approval = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.user.username


class SoldShares(Base):
    user = models.ForeignKey(to=User, on_delete=models.CASCADE)
    share = models.ForeignKey(Share, on_delete=models.CASCADE)
    quantity = models.IntegerField()
    amount_received = models.FloatField()

    def __str__(self):
        return self.user.username
