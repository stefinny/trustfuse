from django.contrib import admin

from .models import Wallet, WalletTransaction


class WalletAdmin(admin.ModelAdmin):

    list_display = ('user', 'amount')
    search_fields = ('user__username',)


class WalletTransactionAdmin(admin.ModelAdmin):

    list_display = ('user', 'amount', 'date_created', 'transaction_type')
    list_filter = ('transaction_type',)
    search_fields = ('user__username', 'date_created__day')


admin.site.register(WalletTransaction, WalletTransactionAdmin)
admin.site.register(Wallet, WalletAdmin)


