from django.dispatch import receiver
from django.db.models.signals import post_save

from .models import WalletTransaction, Wallet, WalletTransactionType


@receiver(post_save, sender=WalletTransaction)
def update_wallet(sender, instance, created, **kwargs):
    if created:
        wallet = Wallet.objects.get(user=instance.user)
        if instance.transaction_type == WalletTransactionType.INVESTMENT:
            wallet.amount += instance.amount
        elif instance.transaction_type == WalletTransactionType.WITHDRAWAL:
            wallet.amount -= instance.amount
        elif instance.transaction_type == WalletTransactionType.DEPOSIT:
            wallet.amount += instance.amount
        elif instance.transaction_type == WalletTransactionType.SOLD:
            wallet.amount += instance.amount
        wallet.save()
