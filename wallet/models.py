from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import gettext as _


class WalletTransactionType(models.IntegerChoices):

    DEPOSIT = 0, _('Deposit')
    WITHDRAWAL = 1, _('Withdrawal')
    INVESTMENT = 3, _('Investment')
    SOLD = 4, _('Sold')


class Wallet(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    amount = models.FloatField(default=0)

    def __str__(self):
        return f"{self.user.username}'s wallet"


class WalletTransaction(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    amount = models.FloatField(default=0)
    date_created = models.DateTimeField(auto_now=True)
    date_modified = models.DateTimeField(auto_now_add=True)
    transaction_type = models.IntegerField(choices=WalletTransactionType.choices)
