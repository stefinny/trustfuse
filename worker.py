# import logging
# import os
#
# os.environ['DJANGO_SETTINGS_MODULE'] = 'trustfuse.settings'
#
# logger = logging.getLogger(__name__)
# import django
#
# django.setup()
# logger.info("[*] setting up django...")
#
# import finnhub
# import json
# import schedule
# import time
# import plotly
# from plotly import graph_objects as go
# from pusher import Pusher
# import requests
#
# from django.conf import settings
#
# from core.models import Share
#
# pusher = Pusher(
#     app_id=settings.PUSHY_APP_ID,
#     key=settings.PUSHY_APP_KEY,
#     secret=settings.PUSHY_APP_SECRET,
#     cluster=settings.PUSHY_APP_CLUSTER,
#     ssl=True
# )
# times = []
# currencies = ["BTC"]
# prices = {
#     "BTC": []
# }
# for item in currencies:
#     Share.objects.get_or_create(symbol=item, defaults={'name': item, 'current_price': 0})
#
#
# def retrieve_data():
#     current_prices = {}
#
#     for currency in currencies:
#         current_prices[currency] = []
#
#     times.append(time.strftime('%H:%M:%S'))
#
#     api_url = "https://min-api.cryptocompare.com/data/pricemulti?fsyms={}&tsyms=USD".format(",".join(currencies))
#     try:
#         response = json.loads(requests.get(api_url).content)
#     except Exception as e:
#         print(str(e))
#         return
#     print(response)
#     # append new price to list of prices for the graph
#     for currency in currencies:
#         share = Share.objects.get(symbol=currency)
#         price = response[currency]['USD']
#         share.current_price = price
#         share.save()
#         current_prices[currency] = price
#         prices[currency].append(price)
#
#     graph_data = [go.Scatter(
#         x=times[-100:],
#         y=prices.get(currency)[-100:],
#         name="{} Prices".format(currency)
#     ) for currency in currencies]
#     bar_chart_data = [go.Bar(
#         x=currencies,
#         y=list(current_prices.values())
#     )]
#
#     data = {
#         'graph': json.dumps(list(graph_data), cls=plotly.utils.PlotlyJSONEncoder),
#         'bar_chart': json.dumps(list(bar_chart_data), cls=plotly.utils.PlotlyJSONEncoder)
#     }
#     print(data['graph'])
#     try:
#         pusher.trigger('crypto', 'data-updated', data)
#     except Exception as e:
#         print(str(e))
#         pass
#
#
# schedule.every(5).seconds.do(retrieve_data)
# while True:
#     schedule.run_pending()
import json

import websocket


def on_message(ws, message):
    print(' '.join([f"{item['s']}:{item['p']}" for item in json.loads(message)['data']]))


def on_error(ws, error):
    print(error)


def on_close(ws, x, y):
    print("### closed ###")


def on_open(ws):
    ws.send('{"type":"subscribe","symbol":"AAPL"}')
    ws.send('{"type":"subscribe","symbol":"AMZN"}')
    ws.send('{"type":"subscribe","symbol":"BINANCE:BTCUSDT"}')
    ws.send('{"type":"subscribe","symbol":"IC MARKETS:1"}')


if __name__ == "__main__":
    # websocket.enableTrace(True)
    wst = websocket.WebSocketApp(
        "wss://ws.finnhub.io?token=c4pnoniad3icgcueojv0",
        on_message=on_message,
        on_error=on_error,
        on_close=on_close
    )
    wst.on_open = on_open
    wst.run_forever()
