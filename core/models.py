from django.db import models


class Share(models.Model):
    symbol = models.CharField(max_length=100, unique=True)
    name = models.CharField(max_length=100)
    current_price = models.FloatField()
    avatar = models.ImageField(upload_to='shares', default='default.png')
    description = models.TextField(null=True, blank=True, default=None)

    def __str__(self):
        return self.symbol
