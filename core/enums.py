from enum import Enum


class Country(Enum):

    SELECT_COUNTRY = ''
    KENYA = 'KE'
    UGANDA = 'UG'
    TANZANIA = 'TZ'
    NIGERIA = 'NG'

    @classmethod
    def choices(cls):
        return [(key.value, key.name) for key in cls]
