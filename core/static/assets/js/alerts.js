(function($) {
  showSwal = function(type, message) {
    'use strict';
    if (type === 'warning') {
      Swal.fire({
        title: 'Warning!',
        text: message,
        icon: 'warning',

      });
    }
  }

})(jQuery);

