# Generated by Django 3.2.7 on 2021-09-03 16:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='share',
            name='avatar',
            field=models.ImageField(default='default.png', upload_to='shares'),
        ),
        migrations.AddField(
            model_name='share',
            name='description',
            field=models.TextField(blank=True, default=None, null=True),
        ),
    ]
