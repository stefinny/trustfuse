from django.urls import path

from .views import *


app_name = 'core'
urlpatterns = [
    path('', dashboard, name='dashboard'),
    path('charts/', charts, name='charts'),
    path('shares/', shares_view, name='shares'),
    path('shares/search/', search_company, name='search'),
    path('shares/assets/price/', get_asset_price, name='price'),
    path('shares/details/<str:symbol>/', shares_detail, name='details'),
]
