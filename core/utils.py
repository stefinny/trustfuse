from django.contrib.auth.models import User
from django.db.models import Sum


class Account:
    def __init__(self, user: User):
        self.user = user

    def assets(self):
        items = self.user.investment_set.values('share__symbol', 'share__name', 'share__current_price') \
            .order_by('share__symbol') \
            .annotate(total_items=Sum('quantity'), total_paid=Sum('amount'))
        return items

    def sales(self):
        sales = self.user.soldshares_set.values('share__symbol', 'share__name') \
            .order_by('share__symbol') \
            .annotate(total_items=Sum('quantity'), total_paid=Sum('amount_received'))

        return sales
    
    def value(self):
        assets = self.balance()
        sum = 0
        for asset in assets:
            sum += asset['total_items'] * asset['share__current_price']
        return sum

    def balance(self):
        assts = list(self.assets())
        sals = list(self.sales())

        for item in sals:
            for asset in assts:
                if item['share__symbol'] == asset['share__symbol']:
                    asset['total_items'] -= item['total_items']
        return assts
