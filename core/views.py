from django.contrib.auth.decorators import login_required
from django.core import serializers
from django.db.models import Q
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from django.shortcuts import render, redirect, get_object_or_404

from core.models import Share
from core.utils import Account


def dashboard(request):
    assets = Account(request.user)
    context = {
        'assets': assets.balance(),
        'value': assets.value()
    }
    return render(request, 'core/dashboard.html', context=context)


def charts(request):

    context = {
        'api_key': settings.PUSHY_APP_KEY,
        'cluster': settings.PUSHY_APP_CLUSTER,
    }
    return render(request, 'core/trading.html', context=context)


@csrf_exempt
def search_company(request):
    if request.is_ajax():
        search_term = request.POST['q']
        items = Share.objects.filter(Q(symbol__icontains=search_term) | Q(name__icontains=search_term))
        print(f"result: {items}")

        return JsonResponse(data=serializers.serialize('json', items), safe=False)


@csrf_exempt
def get_asset_price(request):
    if request.is_ajax():
        symbol = request.POST['q']
        share = get_object_or_404(Share, symbol=symbol)

        return JsonResponse(data=share.current_price, safe=False)


def shares_view(request):
    s = Share.objects.all()
    context = {
        'shares': s,
    }
    return render(request, 'core/shares_list.html', context=context)


@login_required
def shares_detail(request, symbol):
    try:
        s = Share.objects.get(symbol=symbol)
    except Share.DoesNotExist:
        return redirect('core:shares')
    context = {
        'share': s,
    }
    return render(request, 'core/shares_detail.html', context=context)
