from core.models import Share
from django.contrib import admin


class ShareAdmin(admin.ModelAdmin):
    list_display = ('symbol', 'name', 'current_price')
    search_fields = ('symbol', 'name')


admin.site.register(Share, ShareAdmin)
