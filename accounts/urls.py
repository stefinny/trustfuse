from django.contrib.auth.views import PasswordResetView, PasswordResetDoneView, PasswordResetConfirmView, \
    PasswordResetCompleteView
from django.urls import path
from django.contrib.auth import views as auth_views

from .views import profile, register
from .forms import UserLoginForm


app_name = 'accounts'
urlpatterns = [
    path('login/', auth_views.LoginView.as_view(
        template_name='accounts/login.html',
        authentication_form=UserLoginForm
    ), name='login'),
    path('register/', register, name='register'),
    path('logout/', auth_views.LogoutView.as_view(), name='logout'),
    path('profile/', profile, name='profile'),

    path('reset/reset_password/',
         PasswordResetView.as_view(template_name='accounts/password_reset_form.html'),
         name='reset_password'),
    path('reset/reset_password_sent/',
         PasswordResetDoneView.as_view(template_name="accounts/password_reset_done.html"),
         name='password_reset_done'),
    path('reset/<uidb64>/<token>',
         PasswordResetConfirmView.as_view(template_name="accounts/password_reset_confirm.html"),
         name='password_reset_confirm'),
    path('reset/reset_password_complete/',
         PasswordResetCompleteView.as_view(template_name="accounts/password_reset_complete.html"),
         name='password_reset_complete'),
]
