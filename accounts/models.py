from django.conf import settings
from django.contrib.auth.models import User
from django.db import models


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    phone_number = models.CharField(max_length=13, unique=True)
    referer = models.ForeignKey(User, on_delete=models.CASCADE, related_name='upline', blank=True, null=True)
    blocked = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.user.username} profile"

    def referral_link(self):
        return f"{settings.SITE_HOST}/auth/register?ref={self.user.username}"
