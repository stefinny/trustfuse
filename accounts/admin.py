from django.contrib import admin

from .models import Profile


class ProfileAdmin(admin.ModelAdmin):

    list_display = ('user', 'phone_number', 'referer')

    search_fields = ('user__username', 'phone_number', 'referer__username')


admin.site.register(Profile, ProfileAdmin)
