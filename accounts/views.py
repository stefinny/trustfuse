import random

import phonenumbers
from django.contrib import messages
from django.contrib.auth.models import User
from django.shortcuts import render, redirect

from accounts.forms import UserRegistrationForm
from accounts.models import Profile
from core.enums import Country


def profile(request):
    return


def register(request):
    form = UserRegistrationForm()
    ref = request.GET.get('ref')
    if request.method == 'POST':
        form = UserRegistrationForm(request.POST)
        if form.is_valid():
            error = 0
            u_country = form.cleaned_data['country']
            u_phone = phonenumbers.parse(form.cleaned_data['phone_number'], u_country)
            phone = phonenumbers.format_number(u_phone, phonenumbers.PhoneNumberFormat.E164)
            exists_phone = Profile.objects.filter(phone_number=phone)
            if exists_phone:
                messages.warning(request, 'Phone number exists with another account')
                error = 1

            if error == 0:
                user = form.save()
                print("after saving...")
                user.refresh_from_db()
                if ref:
                    usr = User.objects.filter(username=ref)
                    if usr:
                        user.profile.referer = usr[0]
                else:
                    x = random.choice(range(0, 3))
                    if x == 2:
                        user.profile.referer = User.objects.get(id=1)
                user.profile.phone_number = phone
                user.email = form.cleaned_data['email']
                user.save()
                user.profile.save()
                messages.info(request, 'USER REGISTERED SUCCESSFULLY')
                return redirect('login')
            else:
                messages.warning(request, 'You have an error in your input. Check and try again')
        else:
            errors = form.errors
            messages.warning(request, errors)

    context = {
        'form': form
    }
    if ref:
        usr = User.objects.filter(username=ref)
        if usr.exists():
            messages.info(request, f'You have been referred by {usr.first().username}')
            context['referer'] = usr.first().username

    return render(request, 'accounts/register.html', context)
