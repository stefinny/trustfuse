from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django import forms
from django.contrib.auth.models import User

from core.enums import Country


class UserLoginForm(AuthenticationForm):
    def __init__(self, *args, **kwargs):
        super(UserLoginForm, self).__init__(*args, **kwargs)

    username = forms.CharField(widget=forms.TextInput(
        attrs={
            'class': 'input-material',
            'placeholder': 'Enter your username'
        }
    ))

    password = forms.CharField(widget=forms.PasswordInput(
        attrs={
            'class': 'input-material',
            'placeholder': 'Enter your password'
        }
    ))


class UserRegistrationForm(UserCreationForm):

    def __init__(self, *args, **kwargs):
        super(UserRegistrationForm, self).__init__(*args, **kwargs)

    username = forms.CharField(widget=forms.TextInput(
        attrs={
            'placeholder': 'Create unique username. No spaces'
        }
    ))
    phone_number = forms.CharField(widget=forms.TextInput(
        attrs={
            'placeholder': 'Enter Phone number for making payments'
        }
    ))
    country = forms.CharField()
    email = forms.EmailField(widget=forms.EmailInput(
        attrs={
            'placeholder': 'Enter Email Address'
        }
    ))
    password1 = forms.CharField(widget=forms.PasswordInput(
        attrs={
            'placeholder': 'Create a strong password',
            'class': 'label-material'
        }
    ))
    password2 = forms.CharField(widget=forms.PasswordInput(
        attrs={
            'placeholder': 'Confirm password'
        }
    ))

    class Meta:
        model = User
        fields = ('country', 'email', 'phone_number', 'username', 'password1', 'password2')
